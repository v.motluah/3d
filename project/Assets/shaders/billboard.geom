#version 330                                                                        
                                                                                    
layout(points) in;                                                                  
layout(triangle_strip) out;                                                         
layout(max_vertices = 4) out;                                                       
                                                                                                                                                   
uniform vec3 Camera;                                                            
uniform float Size;

uniform mat4 Projection;  
uniform mat4 View;
uniform mat4 Model;                                                  
                                                                                    
out vec2 TexCoord;                                                                  
                                                                                    
void main()                                                                         
{                                                                                   
    vec3 Pos = gl_in[0].gl_Position.xyz;                                            
	mat4 mvp = Projection * View * Model;
	
    vec3 dir = normalize(Pos - Camera);                                                                                
    vec3 right = cross(dir, vec3(0.0, 1.0, 0.0)) * Size;                              
      	  
    Pos -= right;                                                                   
    gl_Position = mvp * vec4(Pos, 1.0);                                             
    TexCoord = vec2(0.0, 0.0);                                                      
    EmitVertex();                                                                   
                                                                                    
    Pos.y += Size;                                                        
    gl_Position = mvp * vec4(Pos, 1.0);                                             
    TexCoord = vec2(0.0, 1.0);                                                      
    EmitVertex();                                                                   
                                                                                    
    Pos.y -= Size;                                                        
    Pos += right;                                                                   
    gl_Position = mvp * vec4(Pos, 1.0);                                             
    TexCoord = vec2(1.0, 0.0);                                                      
    EmitVertex();                                                                   
                                                                                    
    Pos.y += Size;                                                        
    gl_Position = mvp * vec4(Pos, 1.0);                                             
    TexCoord = vec2(1.0, 1.0);                                                      
    EmitVertex();                                                                   
                                                                                    
    EndPrimitive();                                                                 
}                                                                                   