#version 330 core
layout(location = 0) in vec3 vertex_position_modelspace;
layout(location = 1) in vec2 vertex_uv;
layout(location = 3) in vec3 vertex_normal;

uniform mat4 projection;
uniform mat4 view;
uniform mat4 model;

out vec3 frag_position;
out vec3 normal;

void main()
{   
	vec4 position = model * vec4(vertex_position_modelspace, 1);
	
	normal = normalize(mat3(transpose(inverse(model))) * vertex_normal);
    frag_position = position.xyz;
	
	gl_Position = projection * view * position;
}
