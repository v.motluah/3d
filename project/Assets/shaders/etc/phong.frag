#version 330 core

out vec3 fragment_color;

in vec3 position;
in vec3 color;
in vec3 normal;

struct BaseLight
{
	vec3 ambient;
	vec3 diffuse;
	vec3 specular;
	vec3 color;
};

struct DirectionLight
{
	BaseLight src;
	vec3 direction;
};

float specularStrength = 0.5f;

uniform DirectionLight source;
uniform vec3 camera;

vec3 Diffuse(BaseLight src, vec3 direction)
{
	return src.diffuse * max(dot(direction, normal), 0.0);
}

vec3 Specular(BaseLight src, vec3 direction)
{
	vec3 viewDir = normalize(camera - position);
	vec3 reflectDir = reflect(direction, normal);
	
	float spec = pow(max(dot(viewDir, reflectDir), 0.0), 32) * specularStrength;
	
	return src.specular * spec;
}

vec3 Light(BaseLight src, vec3 direction)
{
	return src.color * src.ambient + Diffuse(src, -direction) + Specular(src, direction);
}

void main()
{	
	fragment_color = color * Light(source.src, source.direction);
}
