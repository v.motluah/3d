#version 330 core
layout(location = 0) in vec3 vertex_position_modelspace;
layout(location = 1) in vec2 vertex_texture_coords;
layout(location = 3) in vec3 vertex_normal;

uniform mat4 projection;
uniform mat4 view;
uniform mat4 model;

out vec2 texture_coords;
out vec3 normal;
out vec3 position;

void main()
{   
	position = vec3(model * vec4(vertex_position_modelspace, 1));
	texture_coords = vertex_texture_coords;
	normal = transpose(inverse(mat3(model))) * vertex_normal;
    gl_Position = projection * view * model * vec4(vertex_position_modelspace, 1);
}
