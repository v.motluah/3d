#version 330 core
layout(location = 0) in vec3 vertex_position_modelspace;
layout(location = 2) in vec3 vertex_color;
layout(location = 3) in vec3 vertex_normal;

uniform mat4 projection;
uniform mat4 view;
uniform mat4 model;

out vec3 color;

void main()
{   
    gl_Position = projection * view * model * vec4(vertex_position_modelspace, 1);
	color = vertex_color;
}
