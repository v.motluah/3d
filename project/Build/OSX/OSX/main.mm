#import <Foundation/Foundation.h>
#include "engine.h"
#include "AppDelegate.h"

int main(int argc, const char* argv[])
{
    @autoreleasepool
    {
        auto delegate = AppDelegate();
        auto& application = engine::application::instance();
        
        application.set_delegate(&delegate);
        application.on_launched();
    }
    return 0;
}
