#include <windows.h>

#include "AppDelegate.h"

int APIENTRY WinMain(HINSTANCE hInstance,
	HINSTANCE hPrevInstance,
	LPTSTR    lpCmdLine,
	int       nCmdShow){
	auto& application = engine::application::instance();
	auto app_delegate = AppDelegate();

	application.set_delegate(&app_delegate);
	application.on_launched();

	return 0;
}