#include "BillboardComponent.h"

void BillboardComponent::start()
{
	texture = engine::texture2d::load_resource<engine::texture2d>("monster_hellknight.png");

	assert(texture);

	program = engine::shader_program::load_resource<engine::shader_program>("billboard.json");

	assert(program);
	
	std::vector<math::vector3d> vertices = {
		{ 1, 0, 0.1 },
		{ -1, 0, 0.1 }
	};

	engine::gl::generate_VAO(vao, vbo, vertices);
}

void BillboardComponent::stop()
{
	glDeleteVertexArrays(1, &vao);
	glDeleteBuffers(1, &vbo);
}

void BillboardComponent::draw(engine::renderer* r, const math::mat4& t)
{
	auto command = r->add_render_command<engine::arrays_command>();

	command->SetupHandler = [this, t](const engine::render_command* command) {
		auto view = command->View;

		program->use();

		program->set_uniform("Model", t);
		program->set_uniform("View", view->view());
		program->set_uniform("Projection", view->projection());
		program->set_uniform("Camera", view->position());
		program->set_uniform("Size", 1.0f);

		engine::gl::enable_blend();
		engine::gl::set_blend_func(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		engine::gl::bind_texture(texture->texture(), 0);
	};

	command->EndHandler = [](const engine::render_command* command) {
		engine::gl::disable_blend();
		engine::gl::bind_texture(0, 0);
	};

	command->Vao = vao;
	command->Mode = GL_POINTS;
	command->Vertices = 2;
}