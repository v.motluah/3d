#pragma once

#include "engine.h"

class BillboardComponent final : public engine::component
{
public:
	void start() override;
	void stop() override;
	void draw(engine::renderer* r, const math::mat4& t) override;
private:
	engine::shader_program_ptr program;
	engine::texture2d_ptr texture;
	GLuint vao, vbo;
};