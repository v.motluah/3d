#include "ParticleSystem.h"

#include <array>

#define MAX_PARTICLES 1000
#define PARTICLE_TYPE_LAUNCHER 1
#define PARTICLE_TYPE_SHELL 2
#define PARTICLE_TYPE_SECONDARY_SHELL 3

void ParticleSystem::start()
{
	std::array<Particle, MAX_PARTICLES> particles;

	particles[0].Type = PARTICLE_TYPE_LAUNCHER;
	particles[0].Position = { 0, 0, 0.1 };
	particles[0].Velocity = { 0.0f, 0.0, 0.0f };
	particles[0].LifetimeMillis = 0;

	glGenVertexArrays(2, particleVao);

	glGenTransformFeedbacks(2, transformFeedback);
	glGenBuffers(2, particleBuffer);

	for (unsigned int i = 0; i < 2; i++) 
	{
		glBindVertexArray(particleVao[i]);
		glBindTransformFeedback(GL_TRANSFORM_FEEDBACK, transformFeedback[i]);
		glBindBufferBase(GL_TRANSFORM_FEEDBACK_BUFFER, 0, particleBuffer[i]);
		glBindTransformFeedback(GL_TRANSFORM_FEEDBACK, 0);

		glBindBuffer(GL_ARRAY_BUFFER, particleBuffer[i]);
		glBufferData(GL_ARRAY_BUFFER, sizeof(Particle) * MAX_PARTICLES, &particles[0], GL_DYNAMIC_DRAW);
		glBindVertexArray(0);
	}

	updateParticlesProgram = engine::resource::load_resource<engine::shader_program>("particles.json");
	assert(updateParticlesProgram);

	renderParticlesProgram = engine::resource::load_resource<engine::shader_program>("billboard.json");
	assert(renderParticlesProgram);

	randomTexture = std::make_unique<RandomTexture>(1000);

	fireWorksTexture = engine::resource::load_resource<engine::texture2d>("fireworks_red.jpg", GL_RGB);
	assert(fireWorksTexture);

	renderParticlesProgram->use();
	renderParticlesProgram->set_uniform("Size", 0.3f);

	updateParticlesProgram->use();
	//updateParticlesProgram->set_uniform("gLauncherLifetime", 100.0f);
	//updateParticlesProgram->set_uniform("gShellLifetime", 10000.0f);
	//updateParticlesProgram->set_uniform("gSecondaryShellLifetime", 2500.0f);
}

void ParticleSystem::stop()
{
	glDeleteTransformFeedbacks(2, transformFeedback);
	glDeleteBuffers(2, particleBuffer);
	glDeleteVertexArrays(2, particleVao);
}

void ParticleSystem::update(float dt)
{
	time += dt;
	this->dt = dt;
}

bool first = true;

void ParticleSystem::updateParticles(float dt)
{	
	if (m_first)
	{
		glDrawArrays(GL_POINTS, 0, 1);

		m_first = false;
	}
	else 
	{
		glDrawTransformFeedback(GL_POINTS, transformFeedback[currVB]);
	}
}

void ParticleSystem::draw(engine::renderer* r, const math::mat4& t)
{
	auto command = r->add_render_command<engine::transform_feedback_command>();

	command->SetupHandler = [this](const engine::render_command* command) {
		updateParticlesProgram->use();
		//updateParticlesProgram->set_uniform("gTime", time * 1000.0f);
		updateParticlesProgram->set_uniform("gDeltaTimeMillis", dt);

		glEnable(GL_RASTERIZER_DISCARD);
		glActiveTexture(0);
		glBindTexture(GL_TEXTURE_1D, randomTexture->Texture());

		glBindBuffer(GL_ARRAY_BUFFER, particleBuffer[currVB]);
		glBindTransformFeedback(GL_TRANSFORM_FEEDBACK, transformFeedback[currTFB]);

		glEnableVertexAttribArray(0);
		glEnableVertexAttribArray(1);
		glEnableVertexAttribArray(2);
		glEnableVertexAttribArray(3);

		glVertexAttribPointer(0, 1, GL_INT, GL_FALSE, sizeof(Particle), (GLvoid*)offsetof(Particle, Type));
		glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(Particle), (GLvoid*)offsetof(Particle, Position));
		glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, sizeof(Particle), (GLvoid*)offsetof(Particle, Velocity));
		glVertexAttribPointer(3, 1, GL_FLOAT, GL_FALSE, sizeof(Particle), (GLvoid*)offsetof(Particle, LifetimeMillis));

		glBeginTransformFeedback(GL_POINTS);
		updateParticles(dt);
	};

	command->Vao = particleVao[currVB];
	command->Mode = GL_POINTS;
	command->Draw = false;

	command->EndHandler = [this](const engine::render_command* command) {
		glEndTransformFeedback();
		glDisableVertexAttribArray(0);
		glDisableVertexAttribArray(1);
		glDisableVertexAttribArray(2);
		glDisableVertexAttribArray(3);
	};

	command = r->add_render_command<engine::transform_feedback_command>();

	command->SetupHandler = [this, t](const engine::render_command* command) {
		auto view = command->View;

		renderParticlesProgram->use();

		renderParticlesProgram->set_uniform("Model", t);
		renderParticlesProgram->set_uniform("View", view->view());
		renderParticlesProgram->set_uniform("Projection", view->projection());
		renderParticlesProgram->set_uniform("Camera", view->position());

		engine::gl::bind_texture(fireWorksTexture->texture(), 0);

		glDisable(GL_RASTERIZER_DISCARD);

		glEnableVertexAttribArray(0);
		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Particle), (GLvoid*)offsetof(Particle, Position));  // position
	};

	command->Mode = GL_POINTS;
	command->Vao = particleVao[currTFB];
	command->Feedback = transformFeedback[currTFB];

	command->EndHandler = [this](const engine::render_command* command) {
		engine::gl::bind_texture(0, 0);
		glDisableVertexAttribArray(0);

		currVB = currTFB;
		currTFB = (currTFB + 1) & 0x1;
	};
}