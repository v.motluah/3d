#pragma once

#include "engine.h"
#include "../Utils/RandomTexture.h"

struct Particle
{
	math::vector3d Position;
	int Type = 0;
	float LifetimeMillis = 0;
	math::vector3d Velocity;
};

class ParticleSystem final : public engine::component
{
public:
	void start() override;
	void stop() override;
	void update(float dt) override;
	void draw(engine::renderer* r, const math::mat4& t) override;
private:
	void updateParticles(float dt);
private:
	unsigned int currVB = 0;
	unsigned int currTFB = 1;
	GLuint particleVao[2];
	GLuint particleBuffer[2];
	GLuint transformFeedback[2];
	float time = 0;
	float dt = 0;
	bool m_first = true;

	engine::shader_program_ptr updateParticlesProgram;
	engine::shader_program_ptr renderParticlesProgram;
	engine::texture2d_ptr fireWorksTexture;

	std::unique_ptr<RandomTexture> randomTexture;
};
