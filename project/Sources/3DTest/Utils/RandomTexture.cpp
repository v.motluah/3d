#include "RandomTexture.h"

float frand()
{
	int r = rand() % 100;
	return r / 100.0f;
}

RandomTexture::RandomTexture(int size)
{
	math::vector3d* data = new math::vector3d[size];
	for (unsigned int i = 0; i < size; i++) {
		data[i].x = frand();
		data[i].y = frand();
		data[i].z = frand();
	}

	texture = engine::gl::load_texture1d(data, size, GL_RGB, GL_RGB, GL_FLOAT);

	delete[] data;
}
