#pragma once

#include "engine.h"

class RandomTexture final
{
public:
	RandomTexture(int size);
	unsigned int Texture() const { return texture; }
private:
	unsigned texture;
};