#include "CubeComponent.h"
#include "ShaderPhong.h"

void CubeComponent::start()
{
	auto program = engine::resource::load_resource<engine::shader_program>("shaders/phong_texture.json");
	auto shader = engine::shader::create<ShaderPhong>(program);

	shader->Scene = game_object()->scene_view();
	shader->Texture = engine::resource::load_resource<engine::texture2d>("textures/container.jpg");

	Shader = shader;

	updateVao();
}

void CubeComponent::draw(engine::renderer* r, const math::mat4& transform)
{
	auto command = r->add_render_command<engine::triangles_command>();

	command->Shader = Shader;
	command->Transform = transform;
	command->VAO = VAO;
	command->Size = 6 * 6;
}

void CubeComponent::updateVao()
{
	auto size = Size / 2;

	std::vector<engine::gl::v3f_n3f_t2f> vertices(4 * 6);

	vertices[0] = { { -size.x, -size.y, -size.z }, { 0, 1, 0 }, { 0, 0 } };
	vertices[1] = { { size.x, -size.y, -size.z }, { 0, 1, 0 }, { 1, 0 } };
	vertices[2] = { { size.x, -size.y, size.z }, { 0, 1, 0 }, { 1, 1 } };
	vertices[3] = { { -size.x, -size.y, size.z }, { 0, 1, 0 }, { 0, 1 } };

	vertices[4] = { { -size.x, size.y, -size.z }, { 0, 1, 0 }, { 0, 0 } };
	vertices[5] = { { size.x, size.y, -size.z }, { 0, 1, 0 }, { 1, 0 } };
	vertices[6] = { { size.x, size.y, size.z }, { 0, 1, 0 }, { 1, 1 } };
	vertices[7] = { { -size.x, size.y, size.z }, { 0, 1, 0 }, { 0, 1 } };

	vertices[8] = { { -size.x, -size.y, -size.z }, { 0, 1, 0 }, { 0, 0 } };
	vertices[9] = { { size.x, -size.y, -size.z }, { 0, 1, 0 }, { 1, 0 } };
	vertices[10] = { { size.x, size.y, -size.z }, { 0, 1, 0 }, { 1, 1 } };
	vertices[11] = { { -size.x, size.y, -size.z }, { 0, 1, 0 }, { 0, 1 } };

	vertices[12] = { { -size.x, -size.y, size.z }, { 0, 1, 0 }, { 0, 0 } };
	vertices[13] = { { size.x, -size.y, size.z }, { 0, 1, 0 }, { 1, 0 } };
	vertices[14] = { { size.x, size.y, size.z }, { 0, 1, 0 }, { 1, 1 } };
	vertices[15] = { { -size.x, size.y, size.z }, { 0, 1, 0 }, { 0, 1 } };

	vertices[16] = { { -size.x, -size.y, -size.z }, { 0, 1, 0 }, { 0, 0 } };
	vertices[17] = { { -size.x, -size.y, size.z }, { 0, 1, 0 }, { 1, 0 } };
	vertices[18] = { { -size.x, size.y, size.z }, { 0, 1, 0 }, { 1, 1 } };
	vertices[19] = { { -size.x, size.y, -size.z }, { 0, 1, 0 }, { 0, 1 } };

	vertices[20] = { { size.x, -size.y, -size.z }, { 0, 1, 0 }, { 0, 0 } };
	vertices[21] = { { size.x, -size.y, size.z }, { 0, 1, 0 }, { 1, 0 } };
	vertices[22] = { { size.x, size.y, size.z }, { 0, 1, 0 }, { 1, 1 } };
	vertices[23] = { { size.x, size.y, -size.z }, { 0, 1, 0 }, { 0, 1 } };

	std::vector<unsigned> indices(6 * 6);

	indices[0] = 0;
	indices[1] = 1;
	indices[2] = 2;
	indices[3] = 2;
	indices[4] = 3;
	indices[5] = 0;

	indices[6] = 4;
	indices[7] = 5;
	indices[8] = 6;
	indices[9] = 6;
	indices[10] = 7;
	indices[11] = 4;

	indices[12] = 8;
	indices[13] = 9;
	indices[14] = 10;
	indices[15] = 10;
	indices[16] = 11;
	indices[17] = 8;

	indices[18] = 12;
	indices[19] = 13;
	indices[20] = 14;
	indices[21] = 14;
	indices[22] = 15;
	indices[23] = 12;

	indices[24] = 16;
	indices[25] = 17;
	indices[26] = 18;
	indices[27] = 18;
	indices[28] = 19;
	indices[29] = 16;

	indices[30] = 20;
	indices[31] = 21;
	indices[32] = 22;
	indices[33] = 22;
	indices[34] = 23;
	indices[35] = 20;

	engine::gl::generate_VAO(VAO, VertexBuffer, IndexBuffer, vertices, indices);
}