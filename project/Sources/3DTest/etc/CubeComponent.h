#pragma once

#include "engine.h"

class CubeComponent : public engine::component
{
public:
	void start() override;
	void draw(engine::renderer* r, const math::mat4& transform) override;
private:
	unsigned VAO;
	unsigned VertexBuffer;
	unsigned IndexBuffer;

	void updateVao();
public:
	math::vector3d Size;
	math::vector3d Color;

	engine::shader_ptr Shader;

};