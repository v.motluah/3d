#include "PlaneComponent.h"
#include "Ray.h"
#include "ShaderPhong.h"

void PlaneComponent::start()
{
	auto program = engine::resource::load_resource<engine::shader_program>("shaders/phong_texture.json");
	auto shader = engine::shader::create<ShaderPhong>(program);
	
	shader->Scene = game_object()->scene_view();
	shader->Texture = engine::resource::load_resource<engine::texture2d>("textures/container.jpg");

	Shader = shader;
	RayDebug = game_object()->add_component<Ray>();

	updateVao();
}

void PlaneComponent::draw(engine::renderer* r, const math::mat4& transform)
{
	auto command = r->add_render_command<engine::triangles_command>();

	command->Shader = Shader;
	command->Transform = transform;
	command->VAO = VAO;
	command->Size = 6;
}

void PlaneComponent::updateVao() 
{
	auto size = Size / 2;

	std::vector<engine::gl::v3f_n3f_t2f> vertices(4);

	vertices[0] = { { -size.X, 0, -size.Y }, { 0, 1, 0 }, { 0, 0 } };
	vertices[1] = { { size.X, 0, -size.Y }, { 0, 1, 0 }, { 1, 0 } };
	vertices[2] = { { size.X, 0, size.Y }, { 0, 1, 0 }, { 1, 1 } };
	vertices[3] = { { -size.X, 0, size.Y }, { 0, 1, 0 }, { 0, 1 } };

	std::vector<unsigned> indices(6);

	indices[0] = 0;
	indices[1] = 1;
	indices[2] = 2;
	indices[3] = 2;
	indices[4] = 3;
	indices[5] = 0;

	engine::gl::generate_VAO(VAO, VertexBuffer, IndexBuffer, vertices, indices);
}

void PlaneComponent::on_mouse_down(const math::vector2d& location)
{
	auto camera = game_object()->scene_view()->camera();

	RayDebug->Start = camera->position();
	RayDebug->End = camera->ray(location, 100);
}