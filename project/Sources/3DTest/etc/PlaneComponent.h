#pragma once

#include "engine.h"

class Ray;

class PlaneComponent : public engine::component
{
public:
	void start() override;
	void draw(engine::renderer* r, const math::mat4& transform) override;

	void on_mouse_down(const math::vector2d& location) override;
private:
	unsigned VAO;
	unsigned VertexBuffer;
	unsigned IndexBuffer;

	void updateVao();
public:
	math::vector2d Size;
	math::vector3d Color;
private:
	engine::shader_ptr Shader;
	std::shared_ptr<Ray> RayDebug;
};