#include "Ray.h"

void Ray::start()
{
	auto shader = engine::shader::create<engine::shader_position_color>(engine::shader_program::load_resource<engine::shader_program>("shaders/unlit.json"));
	shader->Color = { 1, 0, 0 };

	updateVao();

	Shader = shader;
}

void Ray::draw(engine::renderer* r, const math::mat4& transform)
{
	std::vector<math::vector3d> vertex{ Start, End };

	engine::gl::update_VAO(vao, vertexBuffer, vertex);

	auto command = r->add_render_command<engine::lines_command>();
	   
	command->VAO = vao;
	command->Shader = Shader;
	command->Size = 2;
	command->Mode = GL_LINES;
}

void Ray::updateVao()
{
	std::vector<math::vector3d> vertex{ Start, End };

	engine::gl::generate_VAO(vao, vertexBuffer, vertex);
}