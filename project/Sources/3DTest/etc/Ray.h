#pragma once

#include "engine.h"

class Ray : public engine::component
{
public:
	void start() override;
	void draw(engine::renderer* r, const math::mat4& transform) override;
private:
	unsigned vao;
	unsigned vertexBuffer;

	void updateVao();
private:
	engine::shader_ptr Shader;
public:
	math::vector3d Start;
	math::vector3d End;
};