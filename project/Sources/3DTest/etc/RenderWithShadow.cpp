#include "RenderWithShadow.h"

void RenderWithShadow::render_scene(const engine::scene_ptr& scene)
{
	scene->draw(this);

	auto camera = scene->camera();

	execute_commands(camera);

	clear_commands();
}