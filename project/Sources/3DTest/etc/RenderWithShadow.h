#pragma once

#include "engine.h"

class RenderWithShadow : public engine::renderer
{
public:
	void render_scene(const engine::scene_ptr& scene) override;
};
