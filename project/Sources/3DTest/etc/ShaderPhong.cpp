#include "ShaderPhong.h"

void ShaderPhong::setup(engine::render_command* command)
{
	m_program->use();

	m_program->set_uniform("projection", command->Projection);
	m_program->set_uniform("view", command->View);
	m_program->set_uniform("model", command->Transform);

	assert(Scene);

	auto light = Scene->find_components<engine::direction_light>();
	auto camera = Scene->camera();

	if (light.size() > 0)
	{
		auto front = std::static_pointer_cast<engine::direction_light>(light.front());

		m_program->set_uniform("source.direction", front->get_direction());
		m_program->set_uniform("source.src.ambient", front->get_ambient());
		m_program->set_uniform("source.src.diffuse", front->get_diffuse());
		m_program->set_uniform("source.src.specular", front->get_specular());
		m_program->set_uniform("source.src.color", front->get_color());
	}

	if (Texture)
	{
		engine::gl::active_texture(GL_TEXTURE0);
		engine::gl::bind_texture(Texture->texture());
		m_program->set_uniform("sampler", 0);
	}

	m_program->set_uniform("camera", camera->position());
}