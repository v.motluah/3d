#pragma once

#include "engine.h"

class ShaderPhong : public engine::shader
{
public:
	void setup(engine::render_command* command) override;
public:
	engine::scene_ptr Scene;
	engine::texture2d_ptr Texture;
};