#include "ShaderShadow.h"

void ShaderShadow::setup(engine::render_command* command)
{
	assert(Map);

	m_program->use();

	m_program->set_uniform("projection", command->Projection);
	m_program->set_uniform("view", command->View);
	m_program->set_uniform("model", command->Transform);
	
	Map->bindForWriting();
}