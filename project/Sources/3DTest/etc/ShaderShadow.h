#pragma once

#include "engine.h"
#include "ShadowMap.h"

class ShaderShadow : public engine::shader
{
public:
	void setup(engine::render_command* command) override;
public:
	std::shared_ptr<ShadowMap> Map;
};