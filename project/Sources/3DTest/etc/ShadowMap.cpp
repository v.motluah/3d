#include "ShadowMap.h"

void ShadowMap::create(int w, int h)
{
	buffer = engine::gl::generate_framebuffer();
	texture = engine::gl::create_framebuffer_texture(w, h, GL_DEPTH_COMPONENT);

	engine::gl::bind_framefuffer_texture(buffer, texture, GL_DEPTH_ATTACHMENT, GL_NONE);
}

void ShadowMap::bindForWriting()
{
	engine::gl::bing_framebuffer(buffer);
}

void ShadowMap::bindForReading(GLenum unit)
{
	engine::gl::active_texture(unit);
	engine::gl::bind_texture(texture);
}