#pragma once

#include "engine.h"

class ShadowMap
{
public:
	void create(int w, int h);
	void bindForWriting();
	void bindForReading(GLenum unit);
private:
	GLuint buffer;
	GLuint texture;
};
