#include "AppDelegate.h"
#include "Scene.h"

#define _3D

void AppDelegate::application_launched(engine::application* application)
{
	if (!application->create_window(1024, 768, "3d"))
		exit(0);

	auto scene = engine::scene::create();
	auto camera = std::make_shared<engine::camera>();

#ifdef _3D
	camera->set_mode(engine::projection_mode::perspective);
	camera->set_direction(math::vector3d::front);

	engine::gl::enable_multisample();
#else
	camera->set_mode(engine::projection_mode::ortographic);
#endif
	camera->set_view_port({ 1024, 768 });

	engine::director::instance().run_scene(scene);

	engine::resources_manager::instance().add_resources_folder("Assets/textures");
	engine::resources_manager::instance().add_resources_folder("Assets/scripts");
	engine::resources_manager::instance().add_resources_folder("Assets/shaders");

	scene->set_camera(camera);
	scene->root()->add_component<Scene>();
        
    application->run();
}

void AppDelegate::application_terminated(engine::application* application)
{
    application->shutdown();
}

void AppDelegate::application_enter_background(engine::application* application)
{

}

void AppDelegate::application_enter_foreground(engine::application* application)
{

}


