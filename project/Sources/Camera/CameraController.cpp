#include "CameraController.h"

const unsigned forward_flag = 0x001;
const unsigned backward_flag = 0x002;
const unsigned left_flag = 0x004;
const unsigned right_flag = 0x008;

const int forward_key = 87;
const int backward_key = 83;
const int left_key = 65;
const int right_key = 68;

void CameraController::start()
{
	mouse = engine::application::instance().mouse_location();
}

void CameraController::update(float dt)
{
	auto camera = game_object()->scene_view()->camera();
	auto vaxis = math::quarternion(math::vector3d::up, yaw);
	auto left = math::vector3d::normalize(math::quarternion::rotate(vaxis, math::vector3d::left));
	auto front = math::vector3d::cross(math::vector3d::up, left);

	auto location = engine::application::instance().mouse_location();

	auto delta = location - mouse;
	static const float sens = 10.0f;

	yaw += delta.X * sens * dt;
	pitch -= delta.Y * sens * dt;

	pitch = std::fmax(-60, std::fmin(pitch, 60));

	mouse = location;

	auto window = engine::application::instance().win_size();

	static const int border = 20;

	if (mouse.X >= window.X - border)
		yaw += 100 * dt;
	else if (mouse.X <= border)
		yaw -= 100 * dt;

	auto position = camera->position();
	const int speed = 10;

	if (flags & forward_flag)
		position -= front * speed * dt;
	else if (flags & backward_flag)
		position += front * speed * dt;

	if (flags & left_flag)
		position -= left * speed * dt;
	else if (flags & right_flag)
		position += left * speed * dt;

	front = math::quarternion::rotate(math::quarternion(left, pitch), front);

	camera->set_position(position);
	camera->set_direction(math::vector3d::normalize(front));
}

void CameraController::on_key_pressed(int key)
{
	if (key == forward_key)
		flags |= forward_flag;
	else if (key == backward_key)
		flags |= backward_flag;

	if (key == left_key)
		flags |= left_flag;
	else if (key == right_key)
		flags |= right_flag;
}

void CameraController::on_key_released(int key)
{
	if (key == forward_key)
		flags &= ~forward_flag;
	else if (key == backward_key)
		flags &= ~backward_flag;

	if (key == left_key)
		flags &= ~left_flag;
	else if (key == right_key)
		flags &= ~right_flag;
}
