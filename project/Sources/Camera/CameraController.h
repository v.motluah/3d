#pragma once

#include "engine.h"

class CameraController : public engine::component
{
public:
	void start() override;

	void on_key_pressed(int key) override;
	void on_key_released(int key) override;
	void update(float dt) override;
private:
	unsigned flags = 0x000;
	float pitch = 0;
	float yaw = 0;
	math::vector2d mouse;
};