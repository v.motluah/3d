#include "PBR.h"
#include "ShaderPBR.h"

void PBR::start()
{
	auto sphere = game_object()->add_child()->add_component<engine::mesh_renderer>();
	auto light = game_object()->add_child()->add_component<engine::direction_light>();
	auto shader = engine::shader::create<shaderPBR>(engine::resource::load_resource<engine::shader_program>("shaders/pbr.json"));

	light->set_diffuse(math::vector3d(0.7f, 0.7f, 0.7f));
	light->set_specular(math::vector3d(0.1f, 0.1f, 0.1f));
	light->set_direction(math::vector3d(0, 1, 0));

	sphere->set_shader(shader);
	sphere->set_model(engine::model3d::load_resource<engine::model3d>("models/sphere.fbx"));
	sphere->game_object()->set_position({ 0, 0, -10 });
	sphere->game_object()->set_scale({ 0.01, 0.01, 0.01 });
}