#include "ShaderPBR.h"

void shaderPBR::setup(engine::render_command* command)
{
	m_program->use();

	m_program->set_uniform("projection", command->Projection);
	m_program->set_uniform("view", command->View);
	m_program->set_uniform("model", command->Transform);
}