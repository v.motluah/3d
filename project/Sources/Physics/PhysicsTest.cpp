#include "PhysicsTest.h"

void PhysicsTest::start()
{
	auto collider = game_object()->add_child()->add_component<engine::polygon_collider>();
	std::vector<math::vector2d> vertices = { { 0, 0 }, { 100, 0 }, { 50, 100 } };

	collider->set_material({ 0.01, 0.01 });
	collider->set_vertices(vertices);
    collider->game_object()->set_position({ 300, 300, 0 });
	collider->game_object()->set_rotation({ 0, 0, 40 });
	
	//auto world = game_object()->scene_view()->world();
	//world->setGravity({ 0, -10, 0 });
}
