#include "Scene.h"

#define PARTICLES
//#define USER_CONTROLLED_CAMERA

#if defined(USER_CONTROLLED_CAMERA)
#include "Camera/CameraController.h"
#endif

#if defined(PARTICLES)
#include "3Dtest/Particles/ParticleSystem.h"
#elif defined(BILLBOARDS)
#include "3Dtest/Billboards/BillboardComponent.h"
#endif

void Scene::start()
{
#if defined(USER_CONTROLLED_CAMERA)
	game_object()->add_component<CameraController>();
#endif

#if defined(PARTICLES)
	game_object()->add_component<ParticleSystem>();
#elif defined(BILLBOARDS)	
	game_object()->add_component<BillboardComponent>();
#endif

}
